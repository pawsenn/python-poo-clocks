import constants


class Clock:

    def __init__(self, hour: int, minute: int):
        quotient_h, modulo_h = divmod(hour, constants.MINUTE_IN_HOUR)
        quotient_m, modulo_m = divmod(minute, constants.SECONDS_IN_MINUTE)
        self.__hour = (modulo_h + quotient_m) % constants.MINUTE_IN_HOUR
        self.__minute = modulo_m

    @property
    def hour(self):
        return self.__hour

    @property
    def minute(self):
        return self.__minute

    def __add__(self, other):
        hour, minute = 0, 0
        if isinstance(other, int):
            hour = self.hour
            minute = self.minute + other
        elif isinstance(other, Clock):
            hour = self.hour + other.hour
            minute = self.minute + other.minute
        return Clock(hour, minute)

    def __sub__(self, other):
        hour, minute = 0, 0
        if isinstance(other, int):
            hour = self.hour
            minute = self.minute - other
        elif isinstance(other, Clock):
            hour = self.hour - other.hour
            minute = self.minute - other.minute
        return Clock(hour, minute)

    def __eq__(self, other):
        return self.hour == other.hour and self.minute == other.minute

    def __str__(self):
        return f"{self.hour:02d}:{self.minute:02d}"
